# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.destroy_all
User.create(name: 'Administrador', password: "12312312", city: "Fortaleza", state: "CE", email: "admin@fashionhub.com.br", 
	admin: true, confirmed: true)

["Feminino","Masculino","Infantil","Acessórios","Calçado","Lingerie","Moda Praia","Outros"].each do |cat|
  Category.find_or_create_by(name: cat)
end

["Camisa","Calça","Vestido","Biquini","Roupa de Dormir","Lingerie","Outros"].each do |cat|
  ProductCategory.find_or_create_by(name: cat)
end

[["Amarelo",'#feffa6'],["Azul",'#cef6fd'],["Branco",'#ffffff'],["Cinza",'#eeeeee'],["Laranja",'#fbaf5d'],["Nude",'#ffeed0'],
["Marrom",'#c69c6d'],["Preto",'#000000'],["Rosa",'#fac8de'],["Roxo",'#efc8fa'],["Transparente",'#ffffff'],
["Verde",'#d1f3cd'],["Vermelho",'#e02b32']].each do |color|
  Color.find_or_create_by name: color.first, code: color.last
end

["Único","PP","P","M","G","GG","EXG","34","36","38","40","42","44","46","48","50","52","54"].each do |size|
  Size.find_or_create_by(name: size)
end