# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140307185408) do

  create_table "brand_files", force: true do |t|
    t.integer  "brand_id"
    t.string   "file"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "title"
  end

  add_index "brand_files", ["brand_id"], name: "index_brand_files_on_brand_id", using: :btree

  create_table "carts", force: true do |t|
    t.integer  "store_id"
    t.boolean  "current"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "carts", ["store_id"], name: "index_carts_on_store_id", using: :btree
  add_index "carts", ["store_id"], name: "unique_store_id_on_carts", unique: true, using: :btree

  create_table "categories", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "categories_users", force: true do |t|
    t.integer  "user_id"
    t.integer  "category_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "categories_users", ["category_id"], name: "index_categories_users_on_category_id", using: :btree
  add_index "categories_users", ["user_id"], name: "index_categories_users_on_user_id", using: :btree

  create_table "collections", force: true do |t|
    t.string   "name"
    t.string   "delivery_date"
    t.boolean  "release"
    t.integer  "brand_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "collections", ["brand_id"], name: "index_collections_on_brand_id", using: :btree

  create_table "colors", force: true do |t|
    t.string   "name"
    t.string   "code"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "colors_products", force: true do |t|
    t.integer  "color_id"
    t.integer  "product_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "colors_products", ["color_id"], name: "index_colors_products_on_color_id", using: :btree
  add_index "colors_products", ["product_id"], name: "index_colors_products_on_product_id", using: :btree

  create_table "connections", force: true do |t|
    t.integer  "store_id"
    t.boolean  "accepted"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "brand_id"
    t.integer  "requester_id"
  end

  add_index "connections", ["brand_id"], name: "index_connections_on_brand_id", using: :btree
  add_index "connections", ["store_id"], name: "index_connections_on_store_id", using: :btree

  create_table "favorites", force: true do |t|
    t.integer  "store_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "product_id"
  end

  add_index "favorites", ["product_id"], name: "index_favorites_on_product_id", using: :btree
  add_index "favorites", ["store_id"], name: "index_favorites_on_store_id", using: :btree

  create_table "items", force: true do |t|
    t.integer  "product_id"
    t.integer  "color_id"
    t.integer  "size_id"
    t.integer  "quantity"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "cart_id"
    t.integer  "order_id"
  end

  add_index "items", ["cart_id"], name: "index_items_on_cart_id", using: :btree
  add_index "items", ["color_id"], name: "index_items_on_color_id", using: :btree
  add_index "items", ["order_id"], name: "index_items_on_order_id", using: :btree
  add_index "items", ["product_id"], name: "index_items_on_product_id", using: :btree
  add_index "items", ["size_id"], name: "index_items_on_size_id", using: :btree

  create_table "messages", force: true do |t|
    t.integer  "sender_id"
    t.integer  "recipient_id"
    t.text     "body"
    t.boolean  "opened"
    t.boolean  "last_message"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "orders", force: true do |t|
    t.integer  "store_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "brand_id"
    t.boolean  "pending"
  end

  add_index "orders", ["brand_id"], name: "index_orders_on_brand_id", using: :btree
  add_index "orders", ["store_id"], name: "index_orders_on_store_id", using: :btree

  create_table "product_categories", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "product_samples", force: true do |t|
    t.string   "sample"
    t.integer  "product_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "product_samples", ["product_id"], name: "index_product_samples_on_product_id", using: :btree

  create_table "products", force: true do |t|
    t.string   "name"
    t.string   "code"
    t.decimal  "price"
    t.text     "description"
    t.text     "composition"
    t.integer  "collection_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "model"
    t.integer  "product_category_id"
  end

  add_index "products", ["collection_id"], name: "index_products_on_collection_id", using: :btree
  add_index "products", ["product_category_id"], name: "index_products_on_product_category_id", using: :btree

  create_table "products_sizes", force: true do |t|
    t.integer  "size_id"
    t.integer  "product_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "products_sizes", ["product_id"], name: "index_products_sizes_on_product_id", using: :btree
  add_index "products_sizes", ["size_id"], name: "index_products_sizes_on_size_id", using: :btree

  create_table "sizes", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "store_images", force: true do |t|
    t.integer  "store_id"
    t.string   "location"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "description"
  end

  add_index "store_images", ["store_id"], name: "index_store_images_on_store_id", using: :btree

  create_table "user_covers", force: true do |t|
    t.integer  "user_id"
    t.string   "cover"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "user_covers", ["user_id"], name: "index_user_covers_on_user_id", using: :btree

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
    t.string   "corporate_name"
    t.string   "state"
    t.string   "city"
    t.string   "street"
    t.string   "complement"
    t.string   "district"
    t.string   "cep"
    t.string   "site"
    t.string   "facebook"
    t.string   "twitter"
    t.text     "description"
    t.text     "payment_condition"
    t.string   "responsible"
    t.string   "phones"
    t.string   "cnpj"
    t.string   "state_registration"
    t.text     "working_brands"
    t.integer  "opening_year"
    t.integer  "branches"
    t.string   "about"
    t.boolean  "confirmed"
    t.string   "logo"
    t.boolean  "admin"
    t.text     "get_knowing_us"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
