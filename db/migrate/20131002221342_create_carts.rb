class CreateCarts < ActiveRecord::Migration
  def change
    create_table :carts do |t|
      t.references :store, index: true
      t.boolean :current

      t.timestamps
    end

    add_index :carts, :store_id, unique: true, name: 'unique_store_id_on_carts'
  end
end
