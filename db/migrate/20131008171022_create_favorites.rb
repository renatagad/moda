class CreateFavorites < ActiveRecord::Migration
  def change
    create_table :favorites do |t|
      t.references :store, index: true
      t.references :product, index: true

      t.timestamps
    end
  end
end
