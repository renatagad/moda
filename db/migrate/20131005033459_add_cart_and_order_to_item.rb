class AddCartAndOrderToItem < ActiveRecord::Migration
  def change
    add_reference :items, :cart, index: true
    add_reference :items, :order, index: true
  end
end
