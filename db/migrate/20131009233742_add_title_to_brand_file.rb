class AddTitleToBrandFile < ActiveRecord::Migration
  def change
    add_column :brand_files, :title, :string
  end
end
