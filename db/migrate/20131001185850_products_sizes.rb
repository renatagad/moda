class ProductsSizes < ActiveRecord::Migration
  def change
  	create_table :products_sizes do |t|
      t.references :size, index: true
      t.references :product, index: true

      t.timestamps
    end
  end
end
