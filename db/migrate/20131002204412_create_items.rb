class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.references :product, index: true
      t.references :color, index: true
      t.references :size, index: true
      t.integer :quantity

      t.timestamps
    end
  end
end
