class CreateBrandFiles < ActiveRecord::Migration
  def change
    create_table :brand_files do |t|
      t.references :brand, index: true
      t.string :file

      t.timestamps
    end
  end
end
