class CreateConnections < ActiveRecord::Migration
  def change
    create_table :connections do |t|
      t.references :store, index: true
      t.references :product, index: true
      t.boolean :accepted

      t.timestamps
    end
  end
end
