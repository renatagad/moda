class AddDescriptionToStoreImage < ActiveRecord::Migration
  def change
    add_column :store_images, :description, :string
  end
end
