class RemoveProductFromConnection < ActiveRecord::Migration
  def change
    remove_column :connections, :product_id, :integer
  end
end
