class AddDataToUser < ActiveRecord::Migration
  def change
    add_column :users, :name, :string
    add_column :users, :corporate_name, :string
    add_column :users, :state, :string
    add_column :users, :city, :string
    add_column :users, :street, :string
    add_column :users, :complement, :string
    add_column :users, :district, :string
    add_column :users, :cep, :string
    add_column :users, :site, :string
    add_column :users, :facebook, :string
    add_column :users, :twitter, :string
    add_column :users, :description, :text
    add_column :users, :payment_condition, :text
    add_column :users, :responsible, :string
    add_column :users, :phones, :string
    add_column :users, :cnpj, :string
    add_column :users, :state_registration, :string
    add_column :users, :working_brands, :text
    add_column :users, :opening_year, :integer
    add_column :users, :branches, :integer
    add_column :users, :about, :string
  end
end
