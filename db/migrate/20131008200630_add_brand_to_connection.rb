class AddBrandToConnection < ActiveRecord::Migration
  def change
    add_reference :connections, :brand, index: true
  end
end
