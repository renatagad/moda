class CreateProductSamples < ActiveRecord::Migration
  def change
    create_table :product_samples do |t|
      t.string :sample
      t.references :product, index: true

      t.timestamps
    end
  end
end
