class AddBrandToFavorite < ActiveRecord::Migration
  def change
    add_reference :favorites, :brand, index: true
  end
end
