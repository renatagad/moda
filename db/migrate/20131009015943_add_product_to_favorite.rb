class AddProductToFavorite < ActiveRecord::Migration
  def change
    add_reference :favorites, :product, index: true
  end
end
