class AddGetKnowingUsToUser < ActiveRecord::Migration
  def change
    add_column :users, :get_knowing_us, :text
  end
end
