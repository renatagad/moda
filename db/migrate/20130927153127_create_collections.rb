class CreateCollections < ActiveRecord::Migration
  def change
    create_table :collections do |t|
      t.string :name
      t.string :delivery_date
      t.boolean :release
      t.references :brand, index: true

      t.timestamps
    end
  end
end
