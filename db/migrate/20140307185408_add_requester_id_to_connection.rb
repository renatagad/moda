class AddRequesterIdToConnection < ActiveRecord::Migration
  def change
    add_column :connections, :requester_id, :integer
  end
end
