class RemoveProductFromFavorite < ActiveRecord::Migration
  def change
    remove_column :favorites, :product_id, :integer
  end
end
