class ColorsProducts < ActiveRecord::Migration
  def change
  	create_table :colors_products do |t|
      t.references :color, index: true
      t.references :product, index: true

      t.timestamps
    end
  end
end
