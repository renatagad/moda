class CreateUserCovers < ActiveRecord::Migration
  def change
    create_table :user_covers do |t|
      t.references :user, index: true
      t.string :cover

      t.timestamps
    end
  end
end
