class RemoveBrandFromFavorite < ActiveRecord::Migration
  def change
    remove_column :favorites, :brand_id, :integer
  end
end
