module CollectionsHelper

  def collection_image collection
  	(collection.representative_sample ? collection.representative_sample : 'no-product.jpg')
  end

end
