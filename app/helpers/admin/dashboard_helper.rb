module Admin::DashboardHelper
  def stat_variation(stats, i)
    if i < stats.size && previous = stats[i+1]
      s = stats[i]
      
      capture_haml do
        if s['total'].to_i == 0
          haml_tag 'span.label.label-warning', '-'
        else
          variation = ((s['total'].to_i - previous['total'].to_i) * 100)/s['total'].to_i
          if variation >= 5
            haml_tag 'span.label.label-success', "+#{variation}%"
          elsif variation > 0
            haml_tag 'span.label.label-neutral', "+#{variation}%"
          elsif variation < 0
            haml_tag 'span.label.label-important', "#{variation}%"
          else
            haml_tag 'span.label.label-warning', "0%"
          end
        end
      end
    end
  end
end
