module ApplicationHelper

  def confirm_user(current_user, user)

    if !current_user.nil? && current_user.admin
      simple_form_for user, url: user_approval_admin_dashboard_path, method: :post do |f|
        capture_haml do
      	  haml_tag 'fieldset' do
            haml_tag 'h4.title' do
          	  haml_tag '.pull-left' do
          	    haml_tag 'a.text' do
          	  	  haml_concat "Aprovar Usuário"
          	    end
          	  end
            end
            haml_tag 'row.controls-row span12' do
              haml_tag 'div.input.text.optional' do
          	    haml_tag 'label.text.optional{for: :msg}' do
          	  	  haml_concat "Mensagem"
          	    end
          	    haml_concat text_area_tag('msg', '', class: 'span12') 
          	  end
            end
            haml_tag 'button.btn.btn-medium.btn-primary.pull-left', name: 'reject' do
              haml_concat("Rejeitar")
            end
            haml_tag 'button.btn.btn-medium.btn-primary.pull-right' do
              haml_concat("Aprovar")
            end
          end
        end
      end
    end
  end

  def user_link_to(user, &block)
    if user.store?
      link_to capture(&block), store_path(user.id)
    elsif user.brand?
      link_to capture(&block), brand_path(user.id)
    else
      nil
    end
  end

  def user_link_to(user, opts, &block)
    if user.store?
      link_to capture(&block), store_path(user.id), opts
    elsif user.brand?
      link_to capture(&block), brand_path(user.id), opts
    else
      nil
    end
  end

  def full_url(url)
    "#{request.protocol}#{request.host_with_port}#{url}"
  end
end
