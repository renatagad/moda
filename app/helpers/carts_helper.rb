module CartsHelper

  def total_items items
  	items.map{ |i| i.quantity }.sum()
  end

  def total_items_price items
  	number_to_currency items.map{ |i| i.quantity * i.product.price }.sum()
  end

  def items_releases items
  	map = {true => "Lançamento", false => "Pronta Entrega"}
  	items.map{ |i| map[i.product.collection.release] }.uniq.join ' e '
  end
end
