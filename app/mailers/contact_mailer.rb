class ContactMailer < ActionMailer::Base
  def user_contact(name, email, subject, message)
    @name = name
    @email = email
    @msg = message
    mail(to: 'admin@fashionhub.com.br', from: email, subject: subject)
  end
end
