class ConfirmUserMailer < ActionMailer::Base
  default from: "admin@fashionhub.com.br"
  layout 'email'

  def confirm(user, msg)
    @user = user
    @msg = msg
    mail(to: @user.email, subject: 'Bem-vindo ao FashionHub')
  end

  def refuse(user, msg)
    @user = user
    @msg = msg
    mail(to: @user.email, subject: 'Infelizmente seu registro no FashionHub não pode ser aceito')
  end
end
