class UserWelcomeMailer < ActionMailer::Base
  default from: "admin@fashionhub.com.br"
  layout 'email'

  def welcome(user)
    @name = user.name
    mail(to: user.email, subject: 'Bem-vindo ao FashionHub')
  end

end
