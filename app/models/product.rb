class Product < ActiveRecord::Base
  validates :name, :price, presence: true

  belongs_to :collection
  belongs_to :product_category

  has_and_belongs_to_many :colors
  accepts_nested_attributes_for :colors

  has_and_belongs_to_many :sizes
  accepts_nested_attributes_for :sizes

  has_many :product_samples, dependent: :destroy
  accepts_nested_attributes_for :product_samples, allow_destroy: true

  has_many :items
  has_many :favorites

  def representative_sample
    product_samples.first.sample unless product_samples.empty?
  end

  def favorited_by?(store)
    self.favorites.where(store_id: store.id).count > 0
  end
end
