class Connection < ActiveRecord::Base
  belongs_to :store
  belongs_to :brand
  belongs_to :requester, foreign_key: :requester_id, class_name: 'User'

  scope :any_connection, -> (store_id, brand_id) { where(store_id: store_id, brand_id: brand_id) }
  scope :pending_connections, -> (store_id, brand_id) { any_connection(store_id, brand_id).where(accepted: false).count }
  scope :established_connections, -> (store_id, brand_id) { any_connection(store_id, brand_id).where(accepted: true).count }
end
