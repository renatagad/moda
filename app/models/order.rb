class Order < ActiveRecord::Base
  belongs_to :store
  belongs_to :brand

  has_many :items
  accepts_nested_attributes_for :items

  def total_cost
    self.items.map{ |i| i.product.price * i.quantity }.reduce(&:+)
  end
    
end
