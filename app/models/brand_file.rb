class BrandFile < ActiveRecord::Base
  belongs_to :brand
  validates :title, :file, presence: true

  mount_uploader :file, FileUploader
end
