class Brand < User
  validates :name, :corporate_name, :state, :city, :street, :district, :cep, :email, :responsible, :phones, :cnpj, presence: true
  validate :max_covers, on: :update
  mount_uploader :logo, LogoUploader

  has_many :collections, dependent: :destroy
  has_many :connections, dependent: :destroy
  has_many :orders
  has_many :stores, through: :connections
  has_many :messages
  has_many :brand_files

  def total_products
    self.id ? Product.joins(collection: :brand).where('collections.brand_id = ?', self.id).count : 0
  end

  def total_orders
    self.id ? Order.where(brand_id: self.id).count : 0
  end

  def max_covers
  	if self.user_covers.size > 3
      errors.add(:user_covers, "Só podem haver três capas")
    end
  end

  def is_connected_to?(store)
    Connection.established_connections(store.id, self.id) > 0
  end

  def is_requester?(store)
    not Connection.any_connection(store.id, self.id).where(accepted: false, requester_id: self.id).empty?
  end

  def can_send_message?(store)
    is_connected_to?(store) || store.admin?
  end

  def send_message(store, body)
    Message.create!(sender_id: self.id, recipient_id: store.id, body: body) if can_send_message?(store)
  end

  def has_connection_with(store)
    Connection.any_connection(store.id, self.id).count > 0 || store.admin?
  end

  def send_connection_request(store)
    if store.store? && !has_connection_with(store)
      Connection.create!(store_id: store.id, brand_id: self.id, accepted: false, requester_id: self.id)
    end
  end

  def accept_connection_request(store)
    Connection.where(store_id: store.id, brand_id: self.id, accepted: false).update_all(accepted: true)
  end

  def refuse_connection_request(store)
    Connection.where(store_id: store.id, brand_id: self.id, accepted: false).destroy_all
  end

  def firmed_connections
    connections.where(brand_id: self.id, accepted: true)
  end

  def pending_connections
    connections.where(accepted: false).where('requester_id <> ?', self.id)
  end

  def message_connections
    Store.where(id: firmed_connections.map(&:store_id)).order(:name)
  end
end
