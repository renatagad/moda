class Item < ActiveRecord::Base
  belongs_to :product
  belongs_to :color
  belongs_to :size
  belongs_to :cart
  belongs_to :order

  validates :product, :color, :size, presence: true
  validates :quantity, numericality: { greater_than: 0 }
end
