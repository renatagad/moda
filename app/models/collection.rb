class Collection < ActiveRecord::Base
  validates :name, presence: true

  belongs_to :brand
  has_many :products, dependent: :destroy

  def representative_sample
    products.first.representative_sample unless products.empty?
  end
end
