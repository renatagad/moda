class Cart < ActiveRecord::Base
  belongs_to :store

  has_many :items
  accepts_nested_attributes_for :items

  def total_items
    self.items.sum(:quantity)
  end

  def total_cost
    self.items.map{ |i| i.product.price * i.quantity }.reduce(&:+)
  end

  def brand_items
    self.items.group_by{ |i| i.product.collection.brand }
  end
  
  def remove_items_by_product prod_id
    self.items.where(product_id: prod_id).destroy_all
  end

  def items_by_brand brand_id
    items.joins(product: {collection: :brand}).where('users.id = ?', brand_id)
  end

  def update_items params
    transaction do |t|
      update(params)
      removals = params[:items_attributes].select{|i| i[:quantity] == '' }.map{|i| i[:id]}
      self.items.where('id in (?)', removals).destroy_all
      Favorite.where(product_id: self.items.map(&:product_id).uniq).destroy_all
    end
    true
  end

  def create_order brand_id, store_id
    transaction do |t|
      if Connection.where(brand_id: brand_id, store_id: store_id, accepted: true)
        _items = items_by_brand brand_id
        order = Order.create(brand_id: brand_id, store_id: store_id, pending: true)
        _items.update_all order_id: order.id, cart_id: nil
        #Favorite.where(product_id: _items.map(&:product_id).uniq).destroy_all
      end
    end
  end

end
