class ProductSample < ActiveRecord::Base
  belongs_to :product

  mount_uploader :sample, SampleUploader
end
