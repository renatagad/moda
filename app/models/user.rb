class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_and_belongs_to_many :categories
  accepts_nested_attributes_for :categories

  has_many :user_covers, foreign_key: :user_id, dependent: :destroy
  accepts_nested_attributes_for :user_covers, allow_destroy: true

  after_create :send_welcome_mail

  def admin?
    self.type.blank?
  end

  def store?
  	not self.type.blank? and self.type == 'Store'
  end

  def brand?
  	not self.type.blank? and self.type == 'Brand'
  end

  def is_connected_to?(user)
    false
  end

  def is_requester?(user)
    false
  end

  def can_send_message?(user)
    admin?
  end

  def send_message(user, body)
    false
  end

  def has_connection_with(other)
    admin?
  end

  def send_connection_request(user)
    false
  end

  def active_for_authentication?
    super && (admin? || confirmed)
  end

  def inactive_message
    (admin? || confirmed) ? super : :inactive
  end

  def send_welcome_mail
    UserWelcomeMailer.welcome(self).deliver
  end

end
