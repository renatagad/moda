class Message < ActiveRecord::Base

  belongs_to :recipient, class_name: 'User', foreign_key: :user_id
  belongs_to :sender, class_name: 'User', foreign_key: :user_id

  before_create :update_last_message
  after_initialize :default_values
  validates_presence_of :sender_id, :recipient_id, :body
  validate :can_send_message

  scope :last_message_between, -> (user_a, user_b) { where('last_message = true AND ((recipient_id = ? AND sender_id = ?) OR (sender_id = ? AND recipient_id = ?))', user_a, user_b, user_a, user_b).limit(1) }
  scope :threads, -> (user) { where('last_message = true AND (recipient_id = ? OR sender_id = ?)', user, user).order("created_at DESC") }
  scope :unread_messages, -> (user) { where(recipient_id: user, opened: false) }
  scope :conversation_between, -> (user_a, user_b) { where('(recipient_id = ? AND sender_id = ?) OR (sender_id = ? AND recipient_id = ?)', user_a, user_b, user_a, user_b).order('created_at DESC') }

  def excerpt(truncate_size=200)
    if body.size < truncate_size
      body.gsub(/\n/, ' ')
    else
      body.gsub(/\n/, ' ')[0..truncate_size] + '...'
    end
  end

  def self.total_unread(user)
    Message.unread_messages(user).count
  end

  protected
    def update_last_message
      old_message = Message.last_message_between(self.sender_id, self.recipient_id).first

      if old_message
        old_message.last_message = false
        old_message.save!
      end

      self.last_message = true
    end

    def can_send_message
      errors.add(:base, "Não é possível enviar mensagem para esse usuário.") unless sender_id && recipient_id && User.find(sender_id).can_send_message?(User.find(recipient_id))
    end

  private
    def default_values
      self.opened ||= false
      self.last_message ||= true
    end
end
