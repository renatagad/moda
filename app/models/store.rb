class Store < User
  validates :name, :corporate_name, :cnpj, :responsible, :phones, :state, :city, :street, :district, :cep, :email, :opening_year, :working_brands, presence: true
  validate :max_covers, on: :update #validates :user_covers, length: { maximum: 3 }
  mount_uploader :logo, LogoUploader
  has_one :cart
  has_many :favorites
  has_many :connections
  has_many :brands, through: :connections
  has_many :messages

  has_many :store_images
  accepts_nested_attributes_for :store_images, allow_destroy: true

  def max_covers
  	if self.user_covers.size > 3
      errors.add(:user_covers, "Só podem haver três capas")
    end
  end

  def shopping_cart
  	Cart.find_or_create_by(store: self, current: true)
  end

  def total_orders
    Order.where(store_id: self).count
  end

  def total_favorites
    Favorite.where(store_id: self).count
  end

  def is_connected_to?(brand)
    Connection.established_connections(self.id, brand.id) > 0
  end

  def is_requester?(brand)
    not Connection.any_connection(self.id, brand.id).where(accepted: false, requester_id: self.id).empty?
  end

  def can_send_message?(brand)
    is_connected_to?(brand) || brand.admin?
  end

  def send_message(brand, body)
    Message.create!(sender_id: self.id, recipient_id: brand.id, body: body) if can_send_message?(brand)
  end

  def has_connection_with(brand)
    Connection.any_connection(self.id, brand.id).count > 0 || brand.admin?
  end

  def send_connection_request(brand)
    if brand.brand? && !has_connection_with(brand)
      Connection.create!(store_id: self.id, brand_id: brand.id, accepted: false, requester_id: self.id)
    end
  end

  def accept_connection_request(brand)
    Connection.where(store_id: self.id, brand_id: brand.id, accepted: false).update_all(accepted: true)
  end

  def refuse_connection_request(brand)
    Connection.where(store_id: self.id, brand_id: brand.id, accepted: false).destroy_all
  end

  def firmed_connections
    connections.where(accepted: true)
  end

  def pending_connections
    connections.where(accepted: false).where('requester_id <> ?', self.id)
  end

  def message_connections
    Brand.where(id: firmed_connections.map(&:brand_id)).order(:name)
  end

  def favorite_or_unfavorite_product(product_id)
    if Favorite.where(product_id: product_id, store_id: self.id).count == 0
      Favorite.create(product_id: product_id, store_id: self.id)
    else
      Favorite.where(product_id: product_id, store_id: self.id).destroy_all
    end
  end

end
