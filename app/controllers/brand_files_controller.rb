class BrandFilesController < ApplicationController
  respond_to :html
  before_action :set_brand, only: [:index, :destroy, :create, :new]

  before_filter :check_current_user_authorization, only: [:destroy, :new, :create]
  before_filter :check_current_user_store_connected_to_brand, only: [:index]

  def index
  end

  def new
  	@brand_file = BrandFile.new
  end

  # POST /brands/1/brand_files
  def create
    @brand_file = BrandFile.new(brand_file_params)
    @brand_file.brand = @brand
    flash[:notice] = "Arquivo enviado com sucesso" if @brand_file.save
    respond_with @brand_file, location: new_brand_brand_file_path(@brand)
  end

  # DELETE /brands/1
  # DELETE /brands/1.json
  def destroy
    @brand_file.destroy
    respond_with @brand_file, location: new_brand_brand_file_path(@brand)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_brand
      @brand_file = BrandFile.find(params[:id]) if params[:id]
      @brand = Brand.find(params[:brand_id]) if params[:brand_id]
    end

    def brand_file_params
      params.require(:brand_file).permit(:title, :file)
    end

end
