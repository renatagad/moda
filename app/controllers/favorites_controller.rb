class FavoritesController < ApplicationController
  respond_to :html

  def product
  	if current_user.store?
  	  current_user.favorite_or_unfavorite_product params[:id]
  	end
  	@product = Product.find(params[:id])
  	respond_with(@product, location: brand_collection_product_path(@product.collection.brand, @product.collection, @product))
  end

  def collection
  	if current_user.store?
  	  current_user.favorite_or_unfavorite_product params[:id]
  	end
  	@product = Product.find(params[:id])
  	respond_with(@product.collection, location: brand_collection_path(@product.collection.brand, @product.collection))
  end

  def favorite
    if current_user.store?
      current_user.favorite_or_unfavorite_product params[:id]
    end
    @product = Product.find(params[:id])
    respond_with(@product.collection, location: favorite_detail_path)
  end
end
