class CollectionsController < ApplicationController
  respond_to :html
  before_action :set_collection

  before_filter :check_current_user_authorization, only: [:edit, :update, :destroy, :new, :create]
  before_filter :check_current_user_store_connected_to_brand, only: [:index, :show]

  def index
    @collection = @brand.collections.joins(:products).where(release: true).last
  end

  def show
  end

  def new
    @collection = Collection.new
  end

  def edit
  end

  def create
  	@collection = Collection.new(collection_params)
    @collection.brand = @brand
    flash[:notice] = "Coleção criada com sucesso!" if @collection.save
    respond_with(@brand, @collection, location: new_brand_collection_path(@brand))
  end

  def update
  	flash[:notice] = "Coleção atualizado com sucesso!" if @collection.update(collection_params)
  	respond_with(@brand, @collection, location: edit_brand_collection_path(@brand, @collection))
  end

  def destroy
    @collection.destroy
    respond_with(@brand, @collection, location: new_brand_collection_path(@brand))
  end

  private
    def set_collection
      @collection = Collection.find(params[:id]) if params[:id]
      @brand = Brand.find(params[:brand_id]) if params[:brand_id]
    end

    def collection_params
      params.require(:collection).permit(:name, :delivery_date, :release)
    end
end
