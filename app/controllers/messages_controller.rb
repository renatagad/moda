class MessagesController < ApplicationController
  respond_to :html

  before_filter :authenticate_user!

  def index
    @messages = Message.threads(current_user).page(params[:page] || 1)
  end

  def new
    @connections = current_user.message_connections
    @message = Message.new
  end

  def show
  	@partner = User.find(params[:id])
  	@has_connection = current_user.has_connection_with @partner
  	@first_page = true if !params[:page] || params[:page] == 1

  	if @has_connection
  	  @messages = Message.conversation_between(current_user, @partner).page params[:page]
      @messages.where(recipient_id: current_user.id).update_all(opened: true)
  	else
  	  @messages = []
  	  flash[:error] = "Não há conexão com este usuário!"
  	end
  end

  def create
  	@message = Message.new(message_params)
  	@message.sender_id = current_user.id
    if @message.save
    	flash[:notice] = "Mensagem enviada."
    else
      flash[:error] = @message.errors.full_messages.join('. ')
    end
    redirect_to message_path(@message.recipient_id)
    #respond_with @message, location: dashboard_detail_path #location: message_path(@message.recipient_id)
  end

  private
   def message_params
      params.require(:message).permit(:body, :recipient_id)
    end
end
