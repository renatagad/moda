class SearchController < ApplicationController
  before_filter :authenticate_user!

  def index
  	@results = []
    if current_user.brand?
    	@results = Store.where('name ILIKE ?', "%#{params[:s]}%").where(confirmed: true).page(params[:page] || 1)
    elsif current_user.store?
  	  @results = Brand.where('name ILIKE ?', "%#{params[:s]}%").where(confirmed: true).page(params[:page] || 1)
    else
      @results = User.where('name ILIKE ?', "%#{params[:s]}%").where(confirmed: true).page(params[:page] || 1)
    end
  end
end
