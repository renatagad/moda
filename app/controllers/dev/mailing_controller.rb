class Dev::MailingController < ApplicationController
  layout 'email'
  
  def confirmation
    @resource = Store.first
    @token = "1234"
    render 'devise/mailer/confirmation_instructions'
  end
  
  def password
    @resource = Store.first
    @token = "1234"
    render 'devise/mailer/reset_password_instructions'
  end

  def user_approved
    @user = Store.first
    @msg = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed imperdiet aliquet venenatis. 
            Sed vel imperdiet arcu. Sed ac purus blandit mauris mattis tempus. Cras porta ipsum a est bibendum convallis. 
            Sed semper elit quam, vitae convallis erat tempor non. Donec porttitor nisi eget mauris luctus, sit amet 
            fringilla est porttitor. Donec aliquam ultrices augue. Phasellus nec venenatis ligula. In eget lacus consequat, 
            imperdiet urna eget, ultricies est. Cras sit amet vestibulum libero."
    render 'confirm_user_mailer/confirm'
  end

  def user_refused
    @user = Store.first
    @msg = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed imperdiet aliquet venenatis. 
            Sed vel imperdiet arcu. Sed ac purus blandit mauris mattis tempus. Cras porta ipsum a est bibendum convallis. 
            Sed semper elit quam, vitae convallis erat tempor non. Donec porttitor nisi eget mauris luctus, sit amet 
            fringilla est porttitor. Donec aliquam ultrices augue. Phasellus nec venenatis ligula. In eget lacus consequat, 
            imperdiet urna eget, ultricies est. Cras sit amet vestibulum libero."
    render 'confirm_user_mailer/refuse'
  end

  def contact
    @name = "Joe Doe"
    @email = "joedoe@example.com"
    @msg = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed imperdiet aliquet venenatis. 
            Sed vel imperdiet arcu. Sed ac purus blandit mauris mattis tempus. Cras porta ipsum a est bibendum convallis. 
            Sed semper elit quam, vitae convallis erat tempor non. Donec porttitor nisi eget mauris luctus, sit amet 
            fringilla est porttitor. Donec aliquam ultrices augue. Phasellus nec venenatis ligula. In eget lacus consequat, 
            imperdiet urna eget, ultricies est. Cras sit amet vestibulum libero."
    render 'contact_mailer/user_contact', layout: nil
  end
end
