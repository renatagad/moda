class ConnectionsController < ApplicationController
  respond_to :html

  def request_brand
  	@brand = Brand.find(params[:id])
  	if current_user.store?
  	  flash[:notice] = "Solicitação de conexão enviada." if current_user.send_connection_request @brand
  	end
  	respond_with @brand
  end

  def refuse_store
  	@store = Store.find(params[:id])
  	if current_user.brand?
  	  flash[:notice] = "Solicitação de conexão negada." if current_user.refuse_connection_request @store
  	end
  	respond_with @store, location: dashboard_detail_path
  end

  def accept_store
  	@store = Store.find(params[:id])
  	if current_user.brand?
  	  flash[:notice] = "Solicitação de conexão aprovada." if current_user.accept_connection_request @store
  	end
  	respond_with @store, location: dashboard_detail_path
  end

  def request_store
    @store = Store.find(params[:id])
    if current_user.brand?
      flash[:notice] = "Solicitação de conexão enviada." if current_user.send_connection_request @store
    end
    respond_with @store
  end

  def refuse_brand
    @brand = Brand.find(params[:id])
    if current_user.store?
      flash[:notice] = "Solicitação de conexão negada." if current_user.refuse_connection_request @brand
    end
    respond_with @brand, location: dashboard_detail_path
  end

  def accept_brand
    @brand = Brand.find(params[:id])
    if current_user.store?
      flash[:notice] = "Solicitação de conexão aprovada." if current_user.accept_connection_request @brand
    end
    respond_with @brand, location: dashboard_detail_path
  end

end
