class StoresController < ApplicationController
  respond_to :html
  before_action :set_store, only: [:show, :edit, :update, :destroy]

  before_filter :check_current_user_authorization, only: [:edit, :update, :destroy]

  def index
    @stores = Store.all
  end

  def show
  end

  def new
    @store = Store.new
  end

  def edit
  end

  def create
    @store = Store.new(store_params)
    @store.confirmed = false
    flash[:notice] = "Loja cadastrada com sucesso, aguarde a confimacao por email." if @store.save
    respond_with @store, location: new_user_session_path
    # redirect_to new_user_session_path
  end

  def update
    flash[:notice] = "Loja atualizada com sucesso." if @store.update(store_params)
    respond_with @store
  end

  def destroy
    @store.destroy
    respond_with @store
  end

  private
    def set_store
      @store = Store.find(params[:id])
    end

    def store_params
      params.require(:store).permit(:email, :password, :password_confirmation, :reset_password_token, :name, :corporate_name, :state, :city,
        :street, :complement, :district, :cep, :site, :facebook, :twitter, :description, :payment_condition, :responsible, :phones, :cnpj,
        :state_registration, :working_brands, :opening_year, :branches, :about, :logo, category_ids: [],
        user_covers_attributes: [:id, :cover, :done, :_destroy], store_images_attributes: [:id, :location, :description, :done, :_destroy])
    end
end
