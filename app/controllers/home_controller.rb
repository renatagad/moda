class HomeController < ApplicationController
  def index
    if user_signed_in?
      if current_user.admin?
        render 'admin/dashboard/index'
      else
        render 'dashboard'
      end
    end
  end

  def favorite
    @favorites = current_user.favorites
  end

  def activities
    not_found if !current_user.store?

    brands = current_user.brands.where(connections: {accepted: true}).pluck(:id)
    collections = Collection.joins(:products).where(brand_id: brands)
    files = BrandFile.where(brand_id: brands)
    @activities = [collections, files].flatten.sort_by(&:created_at).reverse
    @activities = Kaminari.paginate_array(@activities).page(params[:page]).per(50)
  end

  def send_contact
    ContactMailer.user_contact(params[:name], params[:email], params[:subject], params[:message]).deliver
    flash[:notice] = "Mensagem enviada. Obrigado pelo seu contato."
    redirect_to contact_path
  end

  def orders
    @orders = (current_user.store? ? Order.where(store: current_user) : Order.where(brand: current_user)).page(params[:page] || 1)
  end

end
