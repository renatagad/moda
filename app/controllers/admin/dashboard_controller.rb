class Admin::DashboardController < ApplicationController

  before_filter :authenticate_user!
  before_filter :check_admin_authorization

  def user_approval
     user = User.find_by_id params[:id]
    if params[:reject]
      ConfirmUserMailer.refuse(user, confirm_params[:msg]).deliver
      user.destroy!
      flash[:notice] = 'Usuário rejeitado e email enviado'
    else
    	user.update_attributes confirmed: true
    	ConfirmUserMailer.confirm(user, confirm_params[:msg]).deliver
    	flash[:notice] = 'Usuário confirmado e email enviado'
    end
    redirect_to admin_root_path
  end

  def refuse_user
  end

  def orders
    @orders = Order.all.page(params[:page] || 1)
  end

  def transactions
    @transactions = Order.connection.execute('SELECT extract(week from created_at) c_week,
                                              extract(year from created_at) c_year, 
                                              COUNT(1) AS total 
                                              FROM orders 
                                              WHERE pending IS FALSE 
                                              GROUP BY c_week, c_year 
                                              ORDER BY c_year, c_week  
                                              LIMIT 12')
    @transactions = @transactions.to_a
  end

  private

    def confirm_params
      params.permit(:msg)
    end

    def check_admin_authorization
      unless current_user.admin?
        redirect_to root_path
        return false
      end
    end

end
