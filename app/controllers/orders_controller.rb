class OrdersController < ApplicationController
  
  before_filter :check_current_user_authorization, only: [:index, :show, :update]

  def index
  	@orders = Order.where(pending: true).page(params[:page] || 1)
  end

  def show
  	@order = Order.first
  end

  def update
    order = Order.find params[:id]
    order.update_attributes(pending: false)
    redirect_to brand_order_path(order.brand, order)
  end

end
