class ApplicationController < ActionController::Base
  protect_from_forgery

  def after_sign_in_path_for(resource)
    stored_location_for(resource) ||
      if resource.is_a?(User)
      	if resource.admin?
          admin_root_path
        else
          dashboard_detail_path
        end
      else
        super
      end
  end

  def authenticate_admin_user!
    if not current_user.admin? 
      flash[:warning] = "This area is restricted to administrators only."
      redirect_to root_path
    end
    authenticate_user!
  end

  def not_found
    raise ActionController::RoutingError.new('Not Found')
  end

  protected

    def check_current_user_authorization
      resource_id = (params[:brand_id] || params[:id]).to_i
      if not signed_in? or current_user.id != resource_id
        render :file => "public/401.html", :status => :unauthorized
        false
      end
    end

    def check_current_user_store_connected_to_brand
      brand_id = (params[:brand_id] || params[:id]).to_i
      if not signed_in? or (current_user.store? and Connection.established_connections(current_user.id, brand_id) == 0)
        render :file => "public/401.html", :status => :unauthorized
        false
      end
    end

    def check_cart_owner
      cart_id = (params[:cart_id] || params[:id]).to_i
      if not signed_in? or (current_user.store? and current_user.shopping_cart.id != cart_id)
        render :file => "public/401.html", :status => :unauthorized
        false
      end
    end
end
