class BrandsController < ApplicationController
  respond_to :html
  before_action :set_brand, only: [:show, :edit, :update, :destroy, :order, :files]

  before_filter :check_current_user_authorization, only: [:edit, :update, :destroy, :files]
  before_filter :check_cart_owner, only: [:order]

  
  # GET /brands
  # GET /brands.json
  def index
    @brands = Brand.all
  end

  # GET /brands/1
  # GET /brands/1.json
  def show
  end

  # GET /brands/new
  def new
    @brand = Brand.new
  end

  # GET /brands/1/edit
  def edit
  end

  # POST /brands
  # POST /brands.json
  def create
    @brand = Brand.new(brand_params)
    @brand.confirmed = false
    flash[:notice] = "Marca cadastrada com sucesso, aguarde a confimacao por email." if @brand.save
    respond_with @brand, location: new_user_session_path
  end

  # PATCH/PUT /brands/1
  # PATCH/PUT /brands/1.json
  def update
    flash[:notice] = "Marca atualizada com sucesso." if @brand.update(brand_params)
    respond_with @brand
  end

  # DELETE /brands/1
  # DELETE /brands/1.json
  def destroy
    @brand.destroy
    respond_with @brand
  end

  # GET /brands/1/files
  def files
    @brand_file = BrandFile.new
  end

  # POST /carts/:cart_id/brands/:id/order
  def order
    @cart.create_order @brand.id, current_user.id
    flash[:notice] = "Compra efetuada com sucesso!"
    redirect_to cart_path @cart
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_brand
      @brand = Brand.find(params[:id])
      @cart = Cart.find(params[:cart_id]) if params[:cart_id]
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def brand_params
      params.require(:brand).permit(:email, :password, :password_confirmation, :reset_password_token, :name, :corporate_name, :state, :city, 
        :street, :complement, :district, :cep, :site, :facebook, :twitter, :description, :payment_condition, :responsible, :phones, :cnpj, 
        :state_registration, :working_brands, :opening_year, :branches, :about, :logo, :get_knowing_us, category_ids: [], 
        user_covers_attributes: [:id, :cover, :done, :_destroy])
    end

end
