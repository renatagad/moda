class ProductsController < ApplicationController
  respond_to :html
  before_action :set_product

  before_filter :check_current_user_authorization, only: [:edit, :update, :destroy, :new, :create]
  before_filter :check_current_user_store_connected_to_brand, only: [:index, :show]
  before_filter :check_cart_owner, only: [:remove]

  # GET /products
  # GET /products.json
  def index
    @products = Product.all
  end

  # GET /products/1
  # GET /products/1.json
  def show
    @cart = current_user.shopping_cart if current_user.type == "Store"
  end

  # GET /products/new
  def new
    @product = Product.new
  end

  # GET /products/1/edit
  def edit
  end

  # POST /products
  # POST /products.json
  def create
    @product = Product.new(product_params)
    @product.collection = @collection
    flash[:notice] = "Produto criado com sucesso!" if @product.save
    respond_with(@brand, @product, location: edit_brand_collection_path(@brand, @collection))
  end

  # PATCH/PUT /products/1
  # PATCH/PUT /products/1.json
  def update
    flash[:notice] = "Coleção atualizado com sucesso!" if @product.update(product_params)
    respond_with(@brand, @collection, location: edit_brand_collection_path(@brand, @collection))
  end

  # DELETE /products/1
  # DELETE /products/1.json
  def destroy
    @product.destroy
    respond_to do |format|
      format.html { redirect_to edit_brand_collection_path(@brand, @collection) }
      format.json { head :no_content }
    end
  end

  # DELETE /carts/:cart_id/products/:id/remove
  def remove
    flash[:notice] = "Produto removido do carrinho"
    cart = Cart.find(params[:cart_id])
    cart.remove_items_by_product(params[:id])
    redirect_to cart_path cart
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product
      @product = Product.find(params[:id]) if params[:id]
      @collection = Collection.find(params[:collection_id]) if params[:collection_id]
      @brand = Brand.find(params[:brand_id]) if params[:brand_id]
    end

    def product_params
      params.require(:product).permit(:name, :code, :price, :description, :composition, :model, :product_category_id ,color_ids: [], size_ids: [], product_samples_attributes: [:id, :sample, :_destroy])
    end
end
