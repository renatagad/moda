class CartsController < ApplicationController
  respond_to :html
  before_action :set_cart
  before_filter :check_cart_owner, only: [:show, :update]

  def show
  end

  def update
    unless cart_params[:items_attributes].empty?
      flash[:notice] = "Carrinho atualizado!" if @cart.update_items(cart_params)
      if params[:command] == 'cart'
        respond_with @cart, location: cart_path(@cart)
      elsif params[:command] == 'brand'
        respond_with @cart, location: brand_path(params[:brand_id])
      elsif params[:command] == 'favorite'
        respond_with @cart, location: favorite_detail_path
      end
    else
      flash[:alert] = "Nenhum item selecionado."
      prod = Product.find params[:cart][:items_attributes].map{ |i| i['product_id'] }.first
      redirect_to brand_collection_product_path(prod.collection.brand, prod.collection, prod)
    end
  end

  private
    def cart_params
      sanitize_cart_params(params.require(:cart).permit(items_attributes: [:id, :quantity, :color_id, :size_id, :product_id]))
    end

    def sanitize_cart_params params
      params['items_attributes'] = params['items_attributes'].select { |y| not y['id'].blank? or not y['quantity'].blank? }
      params
    end

    def set_cart
      @cart = Cart.find(params[:id]) if params[:id]
    end
end
