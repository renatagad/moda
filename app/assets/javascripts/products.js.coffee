# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$ ->
  $('.product-box .product').dotdotdot({
    ellipsis: '... ',
    height: 40
  });
  
  $('.product-description').dotdotdot({
    ellipsis: '... ',
    height: 40,
    after: "a.readmore",
  });
  
  $("a.readmore").click ->
    $(".product-description").trigger("destroy");
    $("a.readmore").remove()
