//= require jquery
//= require jquery_ujs
//= require cocoon
//= require vendor
//= require_tree .

$ ->
  $('.dropdown-menu').click (e) ->
    e.stopPropagation()

  $('#cover-carousel').carousel({
    interval: 5000,
  })

  $('#etalage').etalage({
    thumb_image_width: 410,
    thumb_image_height: 410,
    source_image_width: 800,
    source_image_height: 800,
    zoom_area_width: 515,
    zoom_area_height: 515,
    small_thumbs: 4,
  });

