#encoding: utf-8
namespace :bootstrap do

  desc "Create Categories"
  task :categories => :environment do
    categories = ['Moda masculina', 'Moda feminina', 'Calçados', 'Acessórios', 'Bolsas', 'Moda Praia', 'Festa']
    categories.each do |category_name|
      Category.create!(name: category_name)
    end
  end

  desc "Create Product Categories"
  task :product_categories => :environment do
    categories = ['Moda masculina', 'Moda feminina', 'Calçados', 'Acessórios', 'Bolsas', 'Moda Praia', 'Festa']
    # ['Blusa', 'Camisa', 'Casaco', 'Jaqueta', 'Malha', 'Sueter', 'Short', 'Saia', 'Calça', 'Vestido',
    #   'Lingerie', 'Moda Praia', 'Camiseta', 'Polo', 'Camisa', 'Bermuda', 'Underwear', 'Roupa Esportiva', 'Botas',
    #   'Mocassin', 'Rasteiras', 'Chinelos', 'Sandálias', 'Sneakers', 'Tênis', 'Bolsa', 'Bijuteria', 'Boné', 'Chapéu',
    #   'Carteira', 'Cinto', 'Mochila', 'Mala', 'Relógio', 'Óculos']
    categories.each do |category_name|
      ProductCategory.create!(name: category_name)
    end
  end

  desc "Create Sizes"
  task :sizes => :environment do
    sizes = ['Único', 'PP', 'P', 'M', 'G', 'GG', 'XGG', '33', '34', '35', '36', '37', '38', '39', '40', '41', '42', '43', '44',
      '45', '46', '47', '48', '49', '50', '51', '52', '53', '54']
    sizes.each do |size_name|
      Size.create!(name: size_name)
    end
  end

  desc "Create Colors"
  task :colors => :environment do
    colors = [ ["Amarelo",'#feffa6'], ["Azul",'#cef6fd'], ["Branco",'#ffffff'], ["Cinza",'#eeeeee'],
      ["Laranja",'#fbaf5d'], ["Nude",'#ffeed0'], ["Marrom",'#c69c6d'], ["Preto",'#000000'], ["Rosa",'#fac8de'],
      ["Roxo",'#efc8fa'], ["Transparente",'#ffffff'], ["Verde",'#d1f3cd'], ["Vermelho",'#e02b32'] ]
    colors.each do |color|
      Color.create!(name: color[0], code: color[1])
    end
  end

  desc "Create Admin"
  task :admin => :environment do
    params = {name: "Admin", email: "admin@fashionhub.com.br", password: "123123", admin: true}
    admin = User.new params
    admin.save!
  end

  desc "Create Brands"
  task :brands => :environment do
    params = {name: "Emporio Armani", email: "emporioarmani@fashionhub.com.br", password: "123123", corporate_name:"Emporio Armani SA",
              state: "SP", city: "São Paulo", street: "Rua tal e tal", cnpj: "864543512345", district: "Centro",
              cep: "160666-666", responsible: "John Doe", phones: "(11) 3333-4444",
              logo: File.open(File.join(Rails.root, "app", "assets", "images", "emporio-armani.jpg")),
              category_ids: Category.limit(4).pluck(:id),
              get_knowing_us: 'Indicação de Fulano', confirmed: true}

    brand = Brand.new params
    ["slide-ea-01.jpg","slide-ea-02.jpg","slide-ea-03.jpg"].each do |f|
      brand.user_covers << UserCover.new(cover: File.open(File.join(Rails.root, "app", "assets", "images", f)))
    end
    brand.save!

    params = {name: "Hugo Boss", email: "hugoboss@fashionhub.com.br", password: "123123", corporate_name:"Hugo Boss SA",
              state: "SP", city: "São Paulo", street: "Rua tal e tal", cnpj: "864543512999", district: "Centro",
              cep: "160666-777", responsible: "Bob Doe", phones: "(11) 3333-5555",
              logo: File.open(File.join(Rails.root, "app", "assets", "images", "hugo-boss.jpg")),
              category_ids: Category.limit(4).pluck(:id),
              get_knowing_us: 'Através do Facebook', confirmed: true}

    brand = Brand.new params
    ["slide-hb-01.jpg","slide-hb-02.jpg","slide-hb-03.jpg"].each do |f|
      brand.user_covers << UserCover.new(cover: File.open(File.join(Rails.root, "app", "assets", "images", f)))
    end
    brand.save!

    params = {name: "Burberry", email: "burberry@fashionhub.com.br", password: "123123", corporate_name:"Burberry SA",
              state: "SP", city: "São Paulo", street: "Rua tal e tal", cnpj: "864543512888", district: "Centro",
              cep: "160666-888", responsible: "Diana Doe", phones: "(11) 3333-6666",
              logo: File.open(File.join(Rails.root, "app", "assets", "images", "burberry.jpg")),
              category_ids: Category.limit(4).pluck(:id),
              get_knowing_us: 'Recomendação de parceiros', confirmed: true}

    brand = Brand.new params
    ["slide-bb-01.jpg","slide-bb-02.jpg","slide-bb-03.jpg"].each do |f|
      brand.user_covers << UserCover.new(cover: File.open(File.join(Rails.root, "app", "assets", "images", f)))
    end
    brand.save!

    params = {name: "Dior", email: "dior@fashionhub.com.br", password: "123123", corporate_name:"Dior SA",
              state: "SP", city: "São Paulo", street: "Rua tal e tal", cnpj: "864543512999", district: "Centro",
              cep: "160666-999", responsible: "Sophia Doe", phones: "(11) 3333-7777",
              logo: File.open(File.join(Rails.root, "app", "assets", "images", "dior.jpg")),
              category_ids: Category.limit(4).pluck(:id),
              get_knowing_us: 'Pesquisa no Google', confirmed: true}

    brand = Brand.new params
    ["slide-di-01.jpg","slide-di-02.jpg","slide-di-03.jpg"].each do |f|
      brand.user_covers << UserCover.new(cover: File.open(File.join(Rails.root, "app", "assets", "images", f)))
    end
    brand.save!

    params = {name: "Jules", email: "jules@fashionhub.com.br", password: "123123", corporate_name:"Jules SA",
              state: "SP", city: "São Paulo", street: "Rua tal e tal", cnpj: "864543512000", district: "Centro",
              cep: "160666-000", responsible: "Jules Doe", phones: "(11) 3333-8888",
              logo: File.open(File.join(Rails.root, "app", "assets", "images", "jules.jpg")),
              category_ids: Category.limit(4).pluck(:id),
              get_knowing_us: 'Indicação profissional', confirmed: false}

    brand = Brand.new params
    ["slide-ju-01.jpg","slide-ju-02.jpg","slide-ju-03.jpg"].each do |f|
      brand.user_covers << UserCover.new(cover: File.open(File.join(Rails.root, "app", "assets", "images", f)))
    end
    brand.save!

    params = {name: "Givenchy", email: "givenchy@fashionhub.com.br", password: "123123", corporate_name:"Givenchy SA",
              state: "SP", city: "São Paulo", street: "Rua tal e tal", cnpj: "864543512111", district: "Centro",
              cep: "160666-111", responsible: "Mia Doe", phones: "(11) 3333-0000",
              logo: File.open(File.join(Rails.root, "app", "assets", "images", "givenchy.jpg")),
              category_ids: Category.limit(4).pluck(:id),
              get_knowing_us: 'Pelo Instagram', confirmed: false}

    brand = Brand.new params
    ["slide-gi-01.jpg"].each do |f|
      brand.user_covers << UserCover.new(cover: File.open(File.join(Rails.root, "app", "assets", "images", f)))
    end
    brand.save!

    params = {name: "Kulte", email: "kulte@fashionhub.com.br", password: "123123", corporate_name:"Kulte SA",
              state: "SP", city: "São Paulo", street: "Rua tal e tal", cnpj: "864543512111", district: "Centro",
              cep: "160666-000", responsible: "Kamile Doe", phones: "(11) 3333-0000",
              logo: File.open(File.join(Rails.root, "app", "assets", "images", "kulte.jpg")),
              category_ids: Category.limit(4).pluck(:id),
              get_knowing_us: 'Newsletter', confirmed: false}

    brand = Brand.new params
    ["slide-ku-01.jpg"].each do |f|
      brand.user_covers << UserCover.new(cover: File.open(File.join(Rails.root, "app", "assets", "images", f)))
    end
    brand.save!
  end

  desc "Create Stores"
  task :stores => :environment do
    params = {name: "Topman Brazil", email: "topman@fashionhub.com.br", password: "123123", corporate_name:"Topman SA",
              cnpj: "543693052376", responsible: "Jane Doe", phones: "(99) 9566-4403", state: "RJ", city: "Rio de Janeiro",
              street: "Rua X de Y e Z", district: "Ipanema", cep: "999160-999", opening_year: "1993", branches: 4,
              working_brands:"Emporio Armani, Hugo Boss", category_ids: Category.offset(5).limit(2).pluck(:id),
              logo: File.open(File.join(Rails.root, "app", "assets", "images", "topman.jpg")), confirmed: true }
    store = Store.new params
    store.user_covers << UserCover.new(cover: File.open(File.join(Rails.root, "app", "assets", "images", "cover-tm.jpg")))
    store.save!

    params = {name: "DrJays", email: "drjays@fashionhub.com.br", password: "123123", corporate_name:"DrJays SA",
              cnpj: "543693052376", responsible: "Jill Doe", phones: "(99) 9566-1212", state: "RJ", city: "Rio de Janeiro",
              street: "Rua X de Y e Z", district: "Ipanema", cep: "999160-121", opening_year: "1995", branches: 4,
              working_brands:"Emporio Armani, Hugo Boss", category_ids: Category.offset(5).limit(2).pluck(:id),
              logo: File.open(File.join(Rails.root, "app", "assets", "images", "drjays.jpg")), confirmed: true }
    store = Store.new params
    store.user_covers << UserCover.new(cover: File.open(File.join(Rails.root, "app", "assets", "images", "cover-dj.jpg")))
    store.save!

    params = {name: "Zara", email: "zara@fashionhub.com.br", password: "123123", corporate_name:"Zara SA",
              cnpj: "543693052233", responsible: "Ned Doe", phones: "(99) 9566-2323", state: "RJ", city: "Rio de Janeiro",
              street: "Rua X de Y e Z", district: "Ipanema", cep: "999160-121", opening_year: "1997", branches: 4,
              working_brands:"Emporio Armani, Hugo Boss", category_ids: Category.offset(5).limit(2).pluck(:id),
              logo: File.open(File.join(Rails.root, "app", "assets", "images", "zara.png")), confirmed: true }
    store = Store.new params
    store.user_covers << UserCover.new(cover: File.open(File.join(Rails.root, "app", "assets", "images", "cover-zr.jpg")))
    store.save!

    params = {name: "Chico's", email: "chicos@fashionhub.com.br", password: "123123", corporate_name:"Chico's SA",
              cnpj: "543693052233", responsible: "Frank Doe", phones: "(99) 9566-3434", state: "RJ", city: "Rio de Janeiro",
              street: "Rua X de Y e Z", district: "Ipanema", cep: "999160-345", opening_year: "1999", branches: 4,
              working_brands:"Emporio Armani, Hugo Boss", category_ids: Category.offset(5).limit(2).pluck(:id),
              logo: File.open(File.join(Rails.root, "app", "assets", "images", "chicos.jpg")), confirmed: true }
    store = Store.new params
    store.user_covers << UserCover.new(cover: File.open(File.join(Rails.root, "app", "assets", "images", "cover-ch.jpg")))
    store.save!

    params = {name: "Uniqlo", email: "uniqlo@fashionhub.com.br", password: "123123", corporate_name:"Uniqlo SA",
              cnpj: "543693052999", responsible: "Mir Doe", phones: "(99) 9566-4545", state: "RJ", city: "Rio de Janeiro",
              street: "Rua X de Y e Z", district: "Ipanema", cep: "999160-777", opening_year: "2000", branches: 4,
              working_brands:"Emporio Armani, Hugo Boss", category_ids: Category.offset(5).limit(2).pluck(:id),
              logo: File.open(File.join(Rails.root, "app", "assets", "images", "uniqlo.jpg")), confirmed: false }
    store = Store.new params
    store.user_covers << UserCover.new(cover: File.open(File.join(Rails.root, "app", "assets", "images", "cover-un.png")))
    store.save!

    params = {name: "Citadium", email: "citadium@fashionhub.com.br", password: "123123", corporate_name:"Citadium SA",
              cnpj: "543693052888", responsible: "Nil Doe", phones: "(99) 9566-5656", state: "RJ", city: "Rio de Janeiro",
              street: "Rua X de Y e Z", district: "Ipanema", cep: "999160-888", opening_year: "2002", branches: 4,
              working_brands:"Emporio Armani, Hugo Boss", category_ids: Category.offset(5).limit(2).pluck(:id),
              logo: File.open(File.join(Rails.root, "app", "assets", "images", "citadium.jpg")), confirmed: false }
    store = Store.new params
    store.user_covers << UserCover.new(cover: File.open(File.join(Rails.root, "app", "assets", "images", "cover-ct.png")))
    store.save!

    params = {name: "Zine", email: "zine@fashionhub.com.br", password: "123123", corporate_name:"Zine SA",
              cnpj: "543693052777", responsible: "Elliot Doe", phones: "(99) 9566-6767", state: "RJ", city: "Rio de Janeiro",
              street: "Rua X de Y e Z", district: "Ipanema", cep: "999160-999", opening_year: "2004", branches: 4,
              working_brands:"Emporio Armani, Hugo Boss", category_ids: Category.offset(5).limit(2).pluck(:id),
              logo: File.open(File.join(Rails.root, "app", "assets", "images", "zine.jpg")), confirmed: false }
    store = Store.new params
    store.user_covers << UserCover.new(cover: File.open(File.join(Rails.root, "app", "assets", "images", "cover-zn.jpg")))
    store.save!
  end

  desc "Create Stores Locations"
  task :store_locations => :environment do
    store = Store.find_by_name 'Topman Brazil'
    %w(01 02 03 04).each do |num|
      store.store_images << StoreImage.new(location: File.open(File.join(Rails.root, "app", "assets", "images", "store-topman-#{num}.jpg")))
    end
    store.save!

    store = Store.find_by_name 'Zara'
    %w(01 02 03 04 05).each do |num|
      store.store_images << StoreImage.new(location: File.open(File.join(Rails.root, "app", "assets", "images", "store-zara-#{num}.jpg")))
    end
    store.save!
  end

  desc "Create Collections"
  task :collections => :environment do
    releases = [true, false, true]
    brand = Brand.first
    releases.each_with_index.each do |r, idx|
      Collection.create name: "Coleção #{idx}", release: r, delivery_date: "01/06/2014", brand: brand
    end
  end

  desc "Create Products"
  task :products => :environment do
    col = Collection.first
    (1..5).each do |idx|
      params = {name: "Produto #{idx}", code:"A10#{idx}", price: 25.49, description: "Esse é do bom!!!",
      composition: "Feito com fio de ovos", color_ids: Color.limit(4).pluck(:id), size_ids: Size.limit(4).pluck(:id)}
      product = Product.new params
      if idx == 1
        ["brand-photo01.jpg","brand-photo02.jpg","brand-photo03.jpg","brand-photo04.jpg"].each do |f|
          product.product_samples << ProductSample.new(sample: File.open(File.join(Rails.root, "app", "assets", "images", f)))
        end
        product.collection = col
      end
      product.save
    end
  end

  desc "Run all bootstrapping tasks"
  task :all => [:categories, :product_categories, :sizes, :colors, :admin, :brands, :stores, :store_locations,
    :collections, :products]

  desc "Clear all bootstrapping data"
  task :clear => :environment do
    Category.destroy_all
    ProductCategory.destroy_all
    Size.destroy_all
    Color.destroy_all
    User.destroy_all
    StoreImage.destroy_all
    Collection.destroy_all
    Product.destroy_all
    Item.destroy_all
    Cart.destroy_all
    Order.destroy_all
  end

end
