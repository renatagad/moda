Fashionhub::Application.routes.draw do
  mount RailsAdmin::Engine => '/fh-admin', as: 'rails_admin'

  devise_for :users
  resources :carts do
    resources :products do
      member do
        delete :remove
      end
    end
    resources :brands do
      member do
        post :order
      end
    end
  end
  resources :stores
  resources :brands do
    resources :brand_files
    resources :collections do
      resources :products
    end
    resources :orders
  end
  resources :favorites do
    member do
      post :product
      post :collection
      post :favorite
    end
  end
  resources :connections, only: [] do
    member do
      post :request_brand
      put :accept_store
      delete :refuse_store
      post :request_store
      put :accept_brand
      delete :refuse_brand
    end
  end
  resources :messages
  resources :search

  root to: 'home#index'

  namespace :admin do
    root to: 'dashboard#index'
    resources :dashboard do
      member do
        post 'user_approval'
      end
      collection do
        get 'orders'
        get 'transactions'
      end
    end
  end

  match "painel", to: "home#dashboard", via: 'get', as: 'dashboard_detail'
  match "loja", to: "home#store", via: 'get', as: 'store_detail'
  match "marca", to: "home#brand", via: 'get', as: 'brand_detail'
  match "favorito", to: "home#favorite", via: 'get', as: 'favorite_detail'
  match "activities", to: "home#activities", via: 'get', as: 'activities_detail'
  match "registro", to: "home#register", via: 'get', as: 'register_detail'
  match "conexao", to: "home#connection", via: 'get', as: 'connection_detail'

  match "empresa", to: "home#about", via: 'get', as: 'about'
  match "termos", to: "home#terms_use", via: 'get', as: 'terms'
  match "privacidade", to: "home#privacy", via: 'get', as: 'privacy'
  match "contato", to: "home#contact", via: 'get', as: 'contact'
  post 'contact', to: 'home#send_contact', as: 'send_contact'
  get 'historico-compras', to: 'home#orders', as: 'history_orders'


  # Tirar quando for implementar a página da Marca
  if Rails.env.development?
    match "email", to: "home#layout_email", via: 'get', as: 'email_detail'
  end
  #match "marca_fechada", :to => "home#brand_closed", :via => 'get', :as => 'brand_closed_detail'
  #match "produto", :to => "home#product", :via => 'get', :as => 'product_detail'
  #match "carrinho", :to => "home#cart", :via => 'get', :as => 'cart_detail'
  #match "colecao", :to => "home#collection", :via => 'get', :as => 'collection_detail'
  #match "upload_arquivo", :to => "home#upload_file", :via => 'get', :as => 'upload_file_detail'
  #match "nova_colecao", :to => "home#create_collection", :via => 'get', :as => 'create_collection_detail'
  #match "editar_colecao", :to => "home#edit_collection", :via => 'get', :as => 'edit_collection_detail'
  #match "novo_produto", :to => "home#create_product", :via => 'get', :as => 'create_product_detail'
  #match "editar_produto", :to => "home#edit_product", :via => 'get', :as => 'edit_product_detail'
  #match "editar", :to => "home#edit_profile", :via => 'get', :as => 'edit_detail'
  #match "editar_loja", :to => "home#edit_profile_store", :via => 'get', :as => 'edit_store_detail'
  #match "buscar", :to => "home#search", :via => 'get', :as => 'search'

  namespace :dev do
    resources :mailing do
      collection do
        get 'confirmation'
        get 'password'
        get 'user_approved'
        get 'user_refused'
        get 'contact'
      end
    end
  end
  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
