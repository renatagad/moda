CarrierWave.configure do |config|
  if Rails.env.production?
    config.fog_credentials = {
      :provider               => 'AWS',                                # required
      :aws_access_key_id      => ENV['S3_KEY'],      # required
      :aws_secret_access_key  => ENV['S3_SECRET']   # required
      #:region                 => 'eu-west-1'  # optional, defaults to 'us-east-1'
    }
    config.fog_directory   = 'staging.fashionhub.com.br'            # required
    config.asset_host      = 'http://staging.fashionhub.com.br.s3-website-us-east-1.amazonaws.com'     # optional, defaults to nil
    #config.fog_public     = false                                   # optional, defaults to true
    #config.fog_attributes = {'Cache-Control'=>'max-age=315576000'}  # optional, defaults to {}
  end
  config.cache_dir = "#{Rails.root}/tmp/uploads"
end